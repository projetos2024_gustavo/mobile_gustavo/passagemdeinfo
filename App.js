import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import TaskList from './src/TaskLista';
import Details from './src/TaskDetails';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="">
        <Stack.Screen name="ListaDeTarefas" component={TaskList}/>
        <Stack.Screen name="DetalhesDasTarefas" component={Details} options={{title: "informações das Tarefas"}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
