import React from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";

const TaskList = ({ navigation }) => {
  const task = [
    {
      id: 1,
      title: "ir ao Mercado",
      date: "2024-02-27",
      time: "10:00",
      address: "Super SS",
    },
    {
      id: 2,
      title: "Fazer exercícios",
      date: "2024-02-27",
      time: "15:00",
      address: "Academia FitLife",
    },
    {
      id: 3,
      title: "Ir ao cinema",
      date: "2024-02-27",
      time: "19:30",
      address: "Cineplex Cinemas",
    },
  ];

  const taskPress = (task) => {
    navigation.navigate("DetalhesDasTarefas", {task});
  };

  return (
    <View>
      <FlatList
        data={task}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};
export default TaskList;
